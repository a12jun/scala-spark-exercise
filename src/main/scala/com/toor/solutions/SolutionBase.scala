package com.toor.solutions

import java.io.{File, FileWriter, IOException}
import java.nio.file.{Files, Paths}

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.rogach.scallop.{ScallopConf, ScallopOption}

abstract class SolutionBase(val session: SparkSession) {

  /**
    * Check if inputFile exists. If not, application will shutdown
    * @param file Path to file
    * @return
    */
  def checkFileExists(file: String): Boolean = {
    if (!Files.exists(Paths.get(file))) {
      println(s"File $file could not be found")
      sys.exit(1)
    }
    true
  }

  /**
    * Reads the given file into a dataframe
    * @param filePath Path to a file containing JSON data
    * @return A dataframe of the file contents
    */
  def readAsDataFrame(filePath: String): DataFrame = {
    if (!checkFileExists(filePath)) {
      throw new IllegalArgumentException("Input file could not be found")
    }
    session.read.json(filePath)
  }

  /**
    * Write the given contents to the file specified in outputFile
    * @param outputFile The file to write to
    * @param content The content to write out
    */
  def writeFile(outputFile: String, content: String): Unit = {
    var fw: FileWriter = null

    try {
      fw = new FileWriter(outputFile)
      fw.write(content)
    }
    catch {
      case e: IOException => {
        println(s"Error writing to $outputFile")
        println("Writing to stdout instead")
        println(content)
      }
    }
    finally {
      if (fw != null) {
        fw.close()
      }
    }
  }

}


abstract class BasicConf(arguments: Seq[String]) extends ScallopConf(arguments) {
  val solutionName : String
  val description : String

  val inputFile
  : ScallopOption[File] = opt[File](required = true, descr = "Path to input file", name = "input-file")
  val outputFile
  : ScallopOption[File] = opt[File](required = true, descr = "File to write result to", name = "output-file")
  validateFileExists(inputFile)
  validateFileIsFile(inputFile)
}