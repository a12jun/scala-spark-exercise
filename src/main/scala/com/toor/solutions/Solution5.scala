package com.toor.solutions

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.rogach.scallop.ScallopOption

class Solution5(session: SparkSession) extends SolutionBase(session = session) {

  def processFile(inputFile: String, outputFile: String, columnName: String, ignoreCase: Boolean): Unit = {
    try {
      val dataFrame = readAsDataFrame(inputFile)
      val result = getWordFrequencies(dataFrame, columnName, ignoreCase)

      writeFile(outputFile, result.mkString(util.Properties.lineSeparator))
    }
    catch {
      case e: IllegalArgumentException => println(e.getMessage)
    }
  }


  /**
    * From the given file, prints the frequencies of every word in the columnName column, across all records
    *
    * @param dataFrame The dataframe of data to use
    * @param columnName The name of the column (field) that should be used
    * @param ignoreCase Whether to ignore case when comparing words to each other
    */
  def getWordFrequencies(dataFrame: DataFrame, columnName: String, ignoreCase: Boolean): Array[(String, Int)] = {
    import dataFrame.sqlContext.implicits._

    dataFrame
      .select(columnName)
      .flatMap(l => {
        if (ignoreCase) l.get(0).toString.toLowerCase.split(" ")
        else l.get(0).toString.split(" ")
      })
      .map(word => (word, 1))
      .rdd
      .reduceByKey((a, b) => a + b)
      .sortBy(e => e._2, ascending = false)
      .collect()
  }
}


/**
  * Solution 5 runner
  */
object Solution5 extends App {
  val conf = new Solution5Conf(args)
  val sparkSession = SparkSession.builder().master("local").getOrCreate()

  val s = new Solution5(sparkSession)
  s.processFile(conf.inputFile.apply.getAbsolutePath, conf.outputFile.apply.getAbsolutePath,
    conf.columnName.apply,
    conf.ignoreCase.apply)
}

class Solution5Conf(arguments: Seq[String]) extends BasicConf(arguments = arguments) {
  override val solutionName: String = "Solution5"
  override val description: String = "Outputs the frequencies of every word in the 'column-name' column, across all records"
  version(s"$solutionName | $description")

  val columnName
  : ScallopOption[String] = opt[String](required = true, descr = "Column name from which to get unique values", name="column-name")
  val ignoreCase
  : ScallopOption[Boolean] = opt[Boolean](descr = "Ignore the case of the values found in the column", name="ignore-case")
  verify()
}
