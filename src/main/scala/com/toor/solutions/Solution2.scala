package com.toor.solutions

import org.apache.spark.sql.{DataFrame, SparkSession}


/**
  * Solution 2 implementation
  * @param session The SparkSession to use
  */
class Solution2(session: SparkSession) extends SolutionBase(session = session) {

  /**
    * Processes the given file as per solution 2
    */
  def processFile(inputFile: String, outputFile: String) {
    try {
      val df = readAsDataFrame(inputFile)
      val count = countRecords(df)
      writeFile(outputFile, s"$count")
    }
    catch {
      case e: IllegalArgumentException => println(e.getMessage)
    }
  }

  /**
    * Counts the number of records in the given dataframe
    */
  def countRecords(dataFrame: DataFrame): Long = {
    dataFrame.count()
  }
}

/**
  * Solution 2 runner
  */
object Solution2 extends App {
  val conf = new Solution2Conf(args)
  val sparkSession = SparkSession.builder().master("local").getOrCreate()

  val s = new Solution2(sparkSession)
  s.processFile(conf.inputFile.apply.getAbsolutePath, conf.outputFile.apply.getAbsolutePath)
}

class Solution2Conf(arguments: Seq[String]) extends BasicConf(arguments = arguments) {
  override val solutionName: String = "Solution2"
  override val description: String = "Gets the number of records in the given file"
  version(s"$solutionName | $description")
  verify()
}

