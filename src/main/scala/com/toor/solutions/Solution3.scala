package com.toor.solutions

import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.rogach.scallop.ScallopOption

import scala.collection.JavaConversions._

class Solution3(session: SparkSession) extends SolutionBase(session = session) {

  def processFile(inputFile: String, outputFile: String, columnName: String) {
    try {
      val frame = readAsDataFrame(inputFile)
      val result = getUniqueValues(frame, columnName)

      writeFile(outputFile, result.mkString(util.Properties.lineSeparator))
    }
    catch {
      case e: IllegalArgumentException => println(e.getMessage)
    }
  }


  /**
    * Outputs the unique values for the given column in the given dataframe
    * @param dataFrame A dataframe of data
    * @param columnName Name of the column to inspect
    */
  def getUniqueValues(dataFrame: DataFrame, columnName: String): java.util.List[Row] = {
    dataFrame.select(columnName).distinct().collectAsList()
  }
}


/**
  * Solution 3 runner
  */
object Solution3 extends App {
  val conf = new Solution3Conf(args)
  val sparkSession = SparkSession.builder().master("local").getOrCreate()

  val s = new Solution3(sparkSession)
  s.processFile(conf.inputFile.apply.getAbsolutePath, conf.outputFile.apply.getAbsolutePath,
    conf.columnName.apply)
}

class Solution3Conf(arguments: Seq[String]) extends BasicConf(arguments = arguments) {
  override val solutionName: String = "Solution3"
  override val description: String = "Outputs the unique values for the given column"
  version(s"$solutionName | $description")

  val columnName
  : ScallopOption[String] = opt[String](required = true, descr = "Column name from which to get unique values", name="column-name")
  verify()
}
