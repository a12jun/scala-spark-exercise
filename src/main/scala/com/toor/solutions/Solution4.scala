package com.toor.solutions

import org.apache.spark.sql.functions.{count, desc}
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.rogach.scallop.ScallopOption

class Solution4(session: SparkSession) extends SolutionBase (session = session) {

  def processFile(inputFile: String, outputFile: String, columnName: String, numRecords: Int): Unit = {
    try {
      val dataFrame = readAsDataFrame(inputFile)
      val result = getMostFrequentValues(dataFrame, columnName, numRecords)
      writeFile(outputFile, result.map(formatRow).mkString(util.Properties.lineSeparator))
    }
    catch {
      case e: IllegalArgumentException => println(e.getMessage)
    }
  }

  /**
    * From the given dataFrame, returns an Array of Rows that correspond to the top n frequent occurring values in columnName.
    * Each row will consists of a tuple of value / count pairs
    * @param dataFrame The dataframe
    * @param columnName The column name to use
    * @param n The number of rows to take (ordered from most to least frequent)
    */
  def getMostFrequentValues(dataFrame: DataFrame, columnName: String, n: Int): Array[Row] = {
    dataFrame
      .groupBy(columnName)
      .agg(count("*") as s"Num_$columnName")
      .orderBy(desc(s"Num_$columnName"))
      .take(n)
  }

  def formatRow(r: Row): String = {
    s"${r.get(1)},${r.get(0)}"
  }
}


/**
  * Solution 4 runner
  */
object Solution4 extends App {
  val conf = new Solution4Conf(args)
  val sparkSession = SparkSession.builder().master("local").getOrCreate()

  val s = new Solution4(sparkSession)
  s.processFile(conf.inputFile.apply.getAbsolutePath, conf.outputFile.apply.getAbsolutePath,
    conf.columnName.apply,
    conf.numRecords.apply)
}
class Solution4Conf(arguments: Seq[String]) extends BasicConf(arguments = arguments) {
  override val solutionName: String = "Solution4"
  override val description: String = "Outputs an Array of Rows that correspond to the top n frequent occurring values in columnName"
  version(s"$solutionName | $description")

  val columnName
  : ScallopOption[String] = opt[String](required = true, descr = "Column name from which to get unique values", name="column-name")
  val numRecords
  : ScallopOption[Int] = opt[Int](required = true, descr = "Number of values to return", name = "num-records")
  verify()
}