package com.toor.solutions

import org.apache.spark.sql.{DataFrame, SparkSession}


/**
  * Solution 1 implementation
  * @param session The SparkSession to use
  */
class Solution1(session: SparkSession) extends SolutionBase(session) {

  /**
    * Processes the given file as per Solution 1
    */
  def processFile(inputFile: String, outputFile: String) : Unit = {
    try {
      val df = readAsDataFrame(inputFile)
      val schemaString = getSchemaString(df)
      writeFile(outputFile, schemaString)
    }
    catch {
      case e : IllegalArgumentException => println(e.getMessage)
    }
  }

  /**
    * Returns a string representing the schema of the given data frame
    * @param dataFrame
    * @return
    */
  def getSchemaString(dataFrame: DataFrame): String = {
    dataFrame.schema.treeString
  }
}



/**
  * Solution 1 runner
  */
object Solution1 extends App {
  val conf = new Solution1Conf(args)
  val sparkSession = SparkSession.builder().master("local").getOrCreate()

  val s = new Solution1(sparkSession)
  s.processFile(conf.inputFile.apply.getAbsolutePath, conf.outputFile.apply.getAbsolutePath)
}

class Solution1Conf(arguments: Seq[String]) extends BasicConf(arguments = arguments) {
  override val solutionName: String = "Solution1"
  override val description: String = "Gets the schema of the given file"
  version(s"$solutionName | $description")
  verify()
}