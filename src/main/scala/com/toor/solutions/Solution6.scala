package com.toor.solutions

import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.sql.functions.{avg, unix_timestamp}
import org.rogach.scallop.ScallopOption

class Solution6(session: SparkSession) extends SolutionBase(session = session) {
  final val startDateAlias = "startDate"
  final val endDateAlias = "endDate"
  final val dateDifferenceAlias = "dateDifference"
  final val secondsInDay = 86400


  def processFile(inputFile: String, outputFile: String, startDateColumn: String, startDateFormat: String,
                  endDateColumn: String, endDateFormat: String): Unit = {
    try {
      val dataFrame = readAsDataFrame(inputFile)
      val result = getAverageDifferenceBetweenDates(dataFrame, startDateColumn, startDateFormat, endDateColumn, endDateFormat)
      writeFile(outputFile, result.mkString)
    }
    catch {
      case e: IllegalArgumentException => println(e.getMessage)
    }
  }


  def getAverageDifferenceBetweenDates(dataFrame: DataFrame, startDateColumn: String, startDateFormat: String,
                                       endDateColumn: String, endDateFormat: String): Array[Row] = {

    // initial filter to remove records without dates
    val df = dataFrame.filter(dataFrame(startDateColumn).isNotNull && dataFrame(endDateColumn).isNotNull)

    // create a frame with the two date columns in unix timestamp format
    var dates = df.select(
        unix_timestamp(df.col(startDateColumn), startDateFormat) as startDateAlias,
        unix_timestamp(df.col(endDateColumn), endDateFormat) as endDateAlias
    )

    // add a column that has the difference value, scaled to days
    dates = dates.withColumn(
      dateDifferenceAlias, (dates.col(endDateAlias) - dates.col(startDateAlias)) / secondsInDay)

    // final average calculation
    dates = dates.select(avg(dateDifferenceAlias))

    dates.collect()
  }
}


/**
  * Solution 6 runner
  */
object Solution6 extends App {
  val conf = new Solution6Conf(args)
  val sparkSession = SparkSession.builder().master("local").getOrCreate()

  val s = new Solution6(sparkSession)
  s.processFile(conf.inputFile.apply.getAbsolutePath, conf.outputFile.apply.getAbsolutePath,
    conf.startDateColumn.apply,
    conf.startDateFormat.apply,
    conf.endDateColumn.apply,
    conf.endDateFormat.apply)
}

class Solution6Conf(arguments: Seq[String]) extends BasicConf(arguments = arguments) {
  override val solutionName: String = "Solution6"
  override val description: String = "Outputs the average time in days, across all records, between the given start and end date columns"
  val defaultDateFormat = "dd/MM/yyyy"
  version(s"$solutionName | $description")

  val startDateColumn
  : ScallopOption[String] = opt[String](required = true, descr = "Start date column name", name="start-date-col")
  val startDateFormat
  : ScallopOption[String] = opt[String](default = Some(defaultDateFormat), descr = "Start date format", name="start-date-format")
  val endDateColumn
  : ScallopOption[String] = opt[String](required = true, descr = "End date column name", name="end-date-col")
  val endDateFormat
  : ScallopOption[String] = opt[String](default = Some(defaultDateFormat), descr = "End date format", name="end-date-format")
  verify()
}