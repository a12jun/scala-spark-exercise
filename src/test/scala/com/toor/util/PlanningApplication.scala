package com.toor.util


case class PlanningApplication(startDate: String, caseText: String, id: Long)

case class DateRecord(startDate: String, endDate: String)