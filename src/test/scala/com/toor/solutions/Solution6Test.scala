package com.toor.solutions

import com.toor.util.DateRecord
import org.apache.spark.sql.DataFrame
import org.scalatest.BeforeAndAfter

class Solution6Test extends SolutionTestBase with BeforeAndAfter {

  var sol6: Solution6 = _
  var dataFrame : DataFrame = _
  import testImplicits._

  before {
    sol6 = new Solution6(sparkSession)
  }


  test("Returns single record containing Double") {
    dataFrame = Seq(
      DateRecord("2018/01/01", "2018/01/03"),
      DateRecord("2018/01/01", "2018/01/03")
    ).toDF()

    val result = sol6.getAverageDifferenceBetweenDates(dataFrame, "startDate", "yyyy/MM/dd", "endDate", "yyyy/MM/dd")

    assert(result.length == 1)
    assert(result(0).schema.fields(0).dataType == org.apache.spark.sql.types.DoubleType)

  }


  test("Gets correct duration for single record") {
    dataFrame = Seq(
      DateRecord("2018/01/01", "2018/01/03")
    ).toDF()

    val result = sol6.getAverageDifferenceBetweenDates(dataFrame, "startDate", "yyyy/MM/dd", "endDate", "yyyy/MM/dd")

    assert(result.exists(e => e.getAs[Double](0) == 2.0))
  }


  test("Gets correct duration for multiple records") {
    dataFrame = Seq(
      DateRecord("2018/01/01", "2018/01/03"),
      DateRecord("2018/01/01", "2018/01/04"),
      DateRecord("2018/01/01", "2018/01/05")
    ).toDF()

    val result = sol6.getAverageDifferenceBetweenDates(dataFrame, "startDate", "yyyy/MM/dd", "endDate", "yyyy/MM/dd")

    assert(result.exists(e => e.getAs[Double](0) == 3.0))

  }
}
