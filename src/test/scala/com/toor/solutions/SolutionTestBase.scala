package com.toor.solutions

import org.apache.spark.sql.{SQLContext, SQLImplicits, SparkSession}
import org.scalatest.{BeforeAndAfterAll, FunSuite}

class SolutionTestBase extends FunSuite with BeforeAndAfterAll { self =>

  var sparkSession : SparkSession = _

  object testImplicits extends SQLImplicits {
    protected override def _sqlContext: SQLContext = self.sparkSession.sqlContext
  }

  /**
    * Setup the SparkSession once for all tests in the suite
    */
  override def beforeAll() {
    sparkSession = SparkSession.builder().master("local").getOrCreate()
    sparkSession.sparkContext.setLogLevel("ERROR")
  }

}
