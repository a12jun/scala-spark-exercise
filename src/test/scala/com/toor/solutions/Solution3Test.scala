package com.toor.solutions

import com.toor.util.PlanningApplication
import scala.collection.JavaConversions._
import org.apache.spark.sql.DataFrame
import org.scalatest.BeforeAndAfter

class Solution3Test extends SolutionTestBase with BeforeAndAfter {
  var sol3 : Solution3 = _
  var dataFrame : DataFrame = _
  import testImplicits._


  before {
    sol3 = new Solution3(sparkSession)
    dataFrame = Seq(
      PlanningApplication("20180318", "This is a case", 1),
      PlanningApplication("20180318", "This is a case", 2),
      PlanningApplication("20180318", "This is a different case", 2),
      PlanningApplication("20180318", "This is another case", 3)
    ).toDF()
  }


  test("Gets the expected unique values for PlanningApplication.id") {
    val result = sol3.getUniqueValues(dataFrame, "id")

    assert(result.length == 3)

    // check we get all the expected values
    assert(result.exists(e => e.get(0) == 1))
    assert(result.exists(e => e.get(0) == 2))
    assert(result.exists(e => e.get(0) == 3))

    // and nothing but the expected values
    assert(!result.exists(e => {
      e.get(0) != 1 && e.get(0) != 2 && e.get(0) != 3
    }))
  }

  test("Gets the correct unique values for PlanningApplication.startDate") {
    val result = sol3.getUniqueValues(dataFrame, "startDate")

    assert(result.length == 1)

    assert(result(0).get(0) == "20180318")
  }

}
