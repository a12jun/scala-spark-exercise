package com.toor.solutions

import com.toor.util.PlanningApplication
import org.apache.spark.sql.DataFrame
import org.scalatest.BeforeAndAfter

class Solution2Test extends SolutionTestBase with BeforeAndAfter {
  var sol2 : Solution2 = _
  var dataFrame: DataFrame = _
  import testImplicits._

  /**
    * Create a new Solution2 for each test case
    */
  before {
    sol2 = new Solution2(sparkSession)
    dataFrame = Seq(
      PlanningApplication("20180319", "case text", 1),
      PlanningApplication("20180319", "case text", 2)
    ).toDF()
  }

  test("Returns correct count for DataFrame containing records") {
    val result = sol2.countRecords(dataFrame)
    val expected = 2

    assert(result == expected)
  }

  test("Returns correct count for empty DataFrame") {
    assert(sol2.countRecords(sparkSession.emptyDataFrame) == 0)
  }

}
