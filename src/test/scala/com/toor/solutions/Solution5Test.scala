package com.toor.solutions

import com.toor.util.PlanningApplication
import org.scalatest.BeforeAndAfter

class Solution5Test extends SolutionTestBase with BeforeAndAfter {

  var sol5 : Solution5 = _
  import testImplicits._

  before {
    this.sol5 = new Solution5(this.sparkSession)
  }


  test("Retrieves correct words") {
    val frame = Seq(
      PlanningApplication("20180319", "one two", 1),
      PlanningApplication("20180319", "two three four", 2)
    ).toDF()

    val result = this.sol5.getWordFrequencies(frame, "caseText", ignoreCase = true)

    // check we get each word
    assert(result.exists(e => e._1 == "one"))
    assert(result.exists(e => e._1 == "two"))
    assert(result.exists(e => e._1 == "three"))
    assert(result.exists(e => e._1 == "four"))

    // and no others
    assert(!result.exists(e => {
      e._1 != "one" && e._1 != "two" && e._1 != "three" && e._1 != "four"
    }))
  }

  test("Ignores case when ignoreCase is true") {
    val frame = Seq(
      PlanningApplication("20180319", "one One ONE", 1)
    ).toDF()
    val result = sol5.getWordFrequencies(frame, "caseText", ignoreCase = true)

    assert(result.length == 1)
    assert(result.exists(e => e._1.equalsIgnoreCase("one")))
  }


  test("Is case-sensitive when ignoreCase is false") {
    val frame = Seq(
      PlanningApplication("20180319", "one One ONE", 1)
    ).toDF()
    val result = sol5.getWordFrequencies(frame, "caseText", ignoreCase = false)

    assert(result.length == 3)
    assert(result.exists(e => e._1.equalsIgnoreCase("one")))
    assert(result.exists(e => e._1.equalsIgnoreCase("One")))
    assert(result.exists(e => e._1.equalsIgnoreCase("ONE")))
  }


  test("Retrieves correct frequencies") {
    val frame = Seq(
      PlanningApplication("20180319", "one", 1),
      PlanningApplication("20180319", "two", 2),
      PlanningApplication("20180319", "two", 2),
      PlanningApplication("20180319", "three", 3),
      PlanningApplication("20180319", "three", 3),
      PlanningApplication("20180319", "three", 3)
    ).toDF()

    val result = sol5.getWordFrequencies(frame, "caseText", ignoreCase = true)

    assert(result.length == 3)

    assert(result.exists(e =>
      e._1.equalsIgnoreCase("one") && e._2 == 1
    ))
    assert(result.exists(e =>
      e._1.equalsIgnoreCase("two") && e._2 == 2
    ))
    assert(result.exists(e =>
      e._1.equalsIgnoreCase("three") && e._2 == 3
    ))
  }
}
