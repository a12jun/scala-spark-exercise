package com.toor.solutions

import com.toor.util.PlanningApplication
import org.apache.spark.sql.DataFrame
import org.scalatest.BeforeAndAfter

class Solution1Test extends SolutionTestBase with BeforeAndAfter { self =>

  var sol1 : Solution1 = _
  var dataFrame: DataFrame = _
  import testImplicits._


  /**
    * Create a new Solution1 for each test case
    */
  before {
    sol1 = new Solution1(sparkSession)
    dataFrame = Seq(
      PlanningApplication("20180319", "case text", 1),
      PlanningApplication("20180319", "case text", 2)
    ).toDF()
  }


  test("Gets correct schema content for data frame") {
    val result = sol1.getSchemaString(dataFrame)
      .replace(" ", "")
      .toLowerCase()

    assert(result.contains("startdate:string"))
    assert(result.contains("casetext:string"))
    assert(result.contains("id:long"))
  }


  test("Gets correct number of lines in schema string") {
    val result = sol1.getSchemaString(dataFrame)
    val expected = 4   // "root" plus one for each field

    assert(result.split('\n').length == expected)
  }


}