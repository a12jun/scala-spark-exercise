package com.toor.solutions

import com.toor.util.PlanningApplication
import org.apache.spark.sql.DataFrame
import org.scalatest.BeforeAndAfter

class Solution4Test extends SolutionTestBase with BeforeAndAfter {
  var sol4 : Solution4 = _
  var dataFrame : DataFrame = _
  import testImplicits._

  before {
    sol4 = new Solution4(sparkSession)
    dataFrame = Seq(
      PlanningApplication("20180318", "One case", 1),

      PlanningApplication("20180318", "Two cases", 2),
      PlanningApplication("20180318", "Two cases", 2),

      PlanningApplication("20180318", "Three cases", 3),
      PlanningApplication("20180318", "Three cases", 3),
      PlanningApplication("20180318", "Three cases", 3),

      PlanningApplication("20180318", "Four cases", 4),
      PlanningApplication("20180318", "Four cases", 4),
      PlanningApplication("20180318", "Four cases", 4),
      PlanningApplication("20180318", "Four cases", 4)
    ).toDF()
  }


  test("Gets correct number of rows") {
    var result = sol4.getMostFrequentValues(dataFrame, columnName = "id", n = 1)
    assert(result.length.equals(1))
    result = sol4.getMostFrequentValues(dataFrame, columnName = "id", n = 2)
    assert(result.length.equals(2))
  }


  test("Gets correct top 1 value") {
    val result = sol4.getMostFrequentValues(dataFrame, columnName = "id", n = 1)
    assert(result(0).get(0) == 4) // the 'id' value column
    assert(result(0).get(1) == 4) // the count (Num_id column)
  }


  test("Returned rows have correct schema") {
    val result = sol4.getMostFrequentValues(dataFrame, columnName = "id", n = 1)
    val schema = result(0).schema

    assert(schema.fields.exists(p => p.name.equalsIgnoreCase("id")))
    assert(schema.fields.exists(p => p.name.equalsIgnoreCase("num_id")))
  }
}
