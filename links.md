
# Links and material

This is a list of links used in the development of this solution.

- https://www.analyticsvidhya.com/blog/2017/01/scala/
- https://spark.apache.org/docs/latest/sql-programming-guide.html#getting-started
- https://spark.apache.org/docs/0.9.1/quick-start.html
- http://spark.apache.org/docs/1.2.1/programming-guide.html#external-datasets
- http://blog.antlypls.com/blog/2016/01/30/processing-json-data-with-sparksql/
- https://spark.apache.org/docs/latest/sql-programming-guide.html#run-sql-on-files-directly
- https://spark.apache.org/docs/latest/sql-programming-guide.html#aggregations
- https://stackoverflow.com/questions/37949494/how-to-count-occurrences-of-each-distinct-value-in-a-column
- https://docs.cloud.databricks.com/docs/spark/1.6/examples/Dataset%20Wordcount.html
- https://stackoverflow.com/questions/30332619/how-to-sort-by-column-in-descending-order-in-spark-sql
- https://stackoverflow.com/questions/30944931/mock-a-spark-rdd-in-the-unit-tests
- https://docs.scala-lang.org/tutorials/scala-with-maven.html