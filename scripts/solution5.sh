#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
pushd ${SCRIPT_DIR}/..

java -cp target/scala-spark-exercise-1.0-SNAPSHOT.jar com.toor.Solution5  \
        src/test/resources/planning-applications-weekly-list.json  \
        results/solution-5.out  \
        CASETEXT  \
        true
popd