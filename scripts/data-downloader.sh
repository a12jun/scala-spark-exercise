#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

remote_file="http://opendata.northumberland.gov.uk/static/datasets/planning-applications-weekly-list/planning-applications-weekly-list.json"
download_dir="${SCRIPT_DIR}/../src/test/resources"
local_file="planning-applications-weekly-list.json"


function download_data_file() {
    curl --output ${download_dir}/${local_file} ${remote_file}
}

# Make directory if it doesn't exist
[[ ! -d ${download_dir} ]]  && mkdir -p ${download_dir}

# Download the file
[[ -f ${local_file} ]]      && echo "Data file already exists" || download_data_file ${remote_file}