# Results
This directory contains the results that I generated locally on my machine.

To generate the results yourselves, please see the Build section of the main [readme file](../README.md).


## solution-X.out files
These contain the results of the given solution.  They can be reproduced by running the solutions.


## Screenshots directory
I have included screenshots of my local build succeeding, as a form of evidence in case you have trouble building the project for any reason.
