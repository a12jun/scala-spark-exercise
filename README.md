# Scala Spark Exercise

## About

### Dependencies

The project was built and developed using:
- Scala; version 2.11
- Java; version 1.8.0_161
- sbt; version 1.1.1

Additional libraries can be found in `build.sbt`

### Build and run instructions

The best way to build the project is using the command line (git bash for Windows, or a standard terminal for Linux).  Please follow the 'first steps' and then see the 'Command Line' section.

#### First steps

1. Clone the repository:
    1. `git clone https://gitlab.com/a12jun/scala-spark-exercise.git`
1. To keep things minimal, the json data file has not been checked into source control. A script has been provided to download it quickly if you do not have it locally;
    1. Run: `scripts/data-downloader.sh`.
    1. This will download it to `${project-root}/src/test/resources/planning-app...`
    1. The instructions given below will assume that the data file is in this location


#### Command line

Make sure you are in the root directory of the project:
```
$  cd <path/to/cloned/project>
```
Now build the project:
```
$  sbt assembly
```

This will build the project to `target`, creating a single fat `jar` file (with all dependencies included).

You can now run any of the solutions individually either:
 - by using the scripts in `scripts/run/`; these contain sensible default argument values.
 - manually, as follows (example below for Solution 1):

```
$  java -cp target/scala-2.11/scala-spark-exercise-1.0-SNAPSHOT.jar \           # The command
      com.toor.solutions.runners.Solution1  \                                     # The main class
      --input-file src/test/resources/planning-applications-weekly-list.json  \   # Argguments
      --output-file results/solution-1.out
```

If you pass the `--help` flag for any solution, it will print a clear usage message.

The output files I generated locally have been included in the `results` directory.

Note that running any of the solutions will overwrite existing output files.


#### IDE
The project was developed using IntelliJ IDEA (version 2017.3.4) with Scala plugin (version: 2017.3.11.1).

The following steps are only guaranteed to work with these dependencies, but they should be similar for other IDE's.

- Open/import the project as a new Scala project
- Navigate to `src/main/scala`.
- The solution to each question is contained within its own scala file. To run one of them, add a 'Run Configuration' that has:
   - Main class:  com.toor.SolutionX
   - Working directory: `path/to/projectRoot`
   - Program arguments: `src/test/resources/planning-applications-weekly-list.json results/solutionX.out`
- Save it as 'Solution X'

You should then be able to "Run Solution X" (or debug it) directly from the IDE.


### Development notes

A list of articles / documentation I used can be found [here](links.md). Initially these covered the basics like setting up IDE / Scala with Spark API, and moved on to more advanced topics.

Regarding project structure, I had tried using `sbt`, but eventually settled on using Maven to manage the project since I had examples of these already, and I didn't want to spend too much time getting up and running.


#### Solution 1

Challenges 
- `printSchema()` only printed to console, so had to figure out how to write the same content to a file

#### Solution 4

Challenges
- Saving the contents to a file; it would be nice to show something in a similar format to `df.show()`
    - Investigating the DataBricks library, looks like it will do the trick
    - Missing `bin/winutils` since I don't have Hadoop installed. Would this work on Linux...?

#### Solution 5

Challenges
- Trying to use `reduceByKey` on a `spark.sql.Dataset` was not available.
- Eventually got a reference to the underlying RDD which allowed me to use `reduceByKey`


### Thoughts

- Unit testing will be light, since most of my functions are either simple wrappers around standard library functions (like writing files).  Regarding Spark context, I am led to believe that the guidelines are to simply run your unit tests within a local Spark context.
- Make sure the application outputs are in line with Spark standard; should I be writing to a Stream instead of a hard file?
- Inject the output stream so I can test with known inputs
- There are a lot of improvements to be made, however in the interest of time I have submitted without implementing the nice-to-haves.

##### Possible features

- Using Streams / generic Writers for I/O
  - This would have made the application much more easily testable as I could have created some dataframes in-memory in the unit tests. For example, creating a frame with a few dummy records, then injecting those into the Solution methods. Then I could ensure that each method returns the expected result.

- Very simple unit tests
  - Time permitting, I would have added tests which simply checked the output files for known values, although the above solution would have been preferable.

- Learning about files in the Spark framework
  - I'd like to move away from using the input file in the local filesystem, and instead learn how Spark deals with files for proper distribution.

- Scala argument parsing library
  - Would have given me some more flexibility with default values, parsing the command line, etc.
